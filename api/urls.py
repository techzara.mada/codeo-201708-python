"""Api urls"""

from django.conf.urls import url
from .views import Events


urlpatterns = [
    # url('^$', views.index, name='api_index'),
    url('events', Events.View.as_view(), name='api_event_picture'),
]
