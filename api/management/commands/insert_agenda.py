from django.core.management.base import BaseCommand
from mongoengine import connect, \
                        Document,\
                        StringField,\
                        EmailField,\
                        URLField, \
                        DateTimeField, \
                        BinaryField
from datetime import datetime


class Event(Document):
    """
        Testdf sdf sdlf skldf sdlfk sdf
         sdf sdf sdflsdfklfdlsk 
         
    """
    title = StringField(max_length=250)
    description = StringField(required=True)
    email = EmailField(required=True)
    link = URLField()
    date_start = DateTimeField(required=True)
    picture = BinaryField()


class Command(BaseCommand):
    help = 'Add fake agenda'

    def add_arguments(self, parser):
        parser.add_argument('num', nargs='+', type=int)

    def handle(self, *args, **options):
        connect('agenda', host='localhost', port=27017)

        picture = ''
        with open('/home/solofo/Pictures/mini.jpg', 'rb') as f:
            picture = f.read()
        
        Event(
            title='Codeo',
            description=':::::::::',
            email='codeo@techzara.com',
            link=None,
            date_start=datetime.strptime("2017-08-17 12:30:1.2", "%Y-%m-%d %H:%M:%S.%f"),
            picture=picture
        ).save()

        num = options['num']
        self.stdout.write("end")
