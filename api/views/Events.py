from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from mongoengine import connect, \
                        Document,\
                        StringField,\
                        EmailField,\
                        URLField, \
                        DateTimeField, \
                        BinaryField
from datetime import datetime
from django.utils import simplejson


class Event(Document):
    dateaujourdhui =  DateTimeField(required=True)
    image= StringField(required=True)
    age = StringField(required=True)
    nometprenom = StringField(required=True)
    email=EmailField(required=True)
    tel = StringField(required=True)
    adresse=StringField(required=True)
    description=StringField(required=True)

    def to_array(self):
        return {
            "dateaujourdhui" : self.dateaujourdhui,
            "image" : self.image,
            "age" : self.age,
            "nometprenom" : self.nometprenom,
            "description" : self.description,
            "tel" : self.tel,
        }

class View(APIView):

    def __init__(self):
        super().__init__()


    def post(self, request):
        connect('agenda', host='localhost', port=27017)
        data = simplejson.loads(request.POST['data'])
        if data.is_valid():
            data.save()
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(data.errors, status=status.HTTP_400_BAD_REQUEST)


    def get(self, request) :
        connect('agenda', host='localhost', port=27017)

        """
        Event(
            title='Codeo',
            description=':::::::::',
            email='codeo@techzara.com',
            link=None,
            date_start=datetime.strptime("2017-08-17 12:30:1.2", "%Y-%m-%d %H:%M:%S.%f"),
        ).save()
        """

        """
        with open('/home/solofo/Pictures/mini.jpg', 'rb') as f:
            return HttpResponse(
                f.read(),
                content_type='image/png'
            )
        
        """

        list = [item.to_array() for item in Event.objects()]
        return Response(list)