from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse


class View(APIView):

    def __init__(self):
        super().__init__()

    def get(self, request):
        with open('/home/solofo/Pictures/mini.jpg', 'rb') as f:
            return HttpResponse(
                f.read(),
                content_type='image/png'
            )
        return Response({
            "message" : "Hello APIView"
        })
