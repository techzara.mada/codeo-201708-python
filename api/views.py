"""Api views"""

from django.shortcuts import render


def index(request):
    """
    Api default view
    """

    return render(request, 'api/index.html', {
        "title" : "Hello Api",
        "days" : [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ],
        "sex" : {
            "M" : "Boy",
            "F" : "Girl",
        }
    })

